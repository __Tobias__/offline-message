package org.tpc.minecraft.offlinemessage.data;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.avaje.ebean.validation.NotNull;

@Entity
@Table(name = "messages")
public class Message implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5451070841987977025L;
	
	@Id
	@NotNull
	@Column(name = "id")
	private UUID id;
	
	@Column(name = "time")
	private long creationTime;
	
	@NotNull
	@Column(name = "message")
	private String message;
	
	public Message() { }
	
	public Message(UUID id, String message) {
		this.id = id;
		this.message = message;
		this.creationTime = System.currentTimeMillis();
	}
	
	public long getCreationTime() {
		return creationTime;
	}
	
	public String getMessage() {
		return message;
	}
	
	public UUID getId() {
		return id;
	}
	
	public void setId(UUID id) {
		this.id = id;
	}
	
	public void setCreationTime(long creationTime) {
		this.creationTime = creationTime;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
}