package org.tpc.minecraft.offlinemessage;

import org.bukkit.ChatColor;

public class Constants {
	
	public static final String CONFIG_MESSAGE_RETRIEVE = "messages.retrieve";
	public static final String CONFIG_MESSAGE_SET = "messages.set";
	public static final String CONFIG_MESSAGE_REMOVE = "messages.remove";
	public static final String CONFIG_MESSAGE_REMOVE_ERROR = "messages.remove-error";
	public static final String CONFIG_MESSAGE_SHOW = "messages.show";
	public static final String CONFIG_MESSAGE_SHOW_ERROR = "messages.show-error";
	
	public static final String DEFAULT_MESSAGE_RETRIEVE = ChatColor.GOLD + "%1s" + ChatColor.WHITE + " is offline. The player wrote: \"" + ChatColor.GRAY + "%2s" + ChatColor.WHITE + "\".";
	public static final String DEFAULT_MESSAGE_SET = ChatColor.GREEN + "The message was set to: \"" + ChatColor.GRAY + "%1s" + ChatColor.GREEN + "\".";
	public static final String DEFAULT_MESSAGE_REMOVE = ChatColor.GREEN + "Removed your offline-message.";
	public static final String DEFAULT_MESSAGE_REMOVE_ERROR = ChatColor.GOLD + "No message to remove!";
	public static final String DEFAULT_MESSAGE_SHOW = ChatColor.GREEN + "Your current message is set to \"" + ChatColor.GRAY + "%1s" + ChatColor.GREEN + "\".";
	public static final String DEFAULT_MESSAGE_SHOW_ERROR = ChatColor.GOLD + "No offline message set!";
	
	public static final String CONFIG_MESSAGE_REMOVE_ON_JOIN = "message.remove-on-join";
	public static final String CONFIG_MESSAGE_SHOW_IF_ONLINE = "message.show-if-online";
	public static final String CONFIG_MESSAGE_STORAGE_TIME = "message.store-time";
	public static final String CONFIG_MESSAGE_CLEANUP_TIME = "message.cleanup-time";
	public static final String CONFIG_STORAGE_FILE = "storage.name";
	
	public static final boolean DEFAULT_MESSAGE_REMOVE_ON_JOIN = false;
	public static final long DEFAULT_MESSAGE_STORAGE_TIME = 1000 * 3600 * 24;
	public static final long DEFAULT_MESSAGE_CLEANUP_TIME = 1000 * 3600 * 5;
	public static final boolean DEFAULT_MESSAGE_SHOW_IF_ONLINE = true;
	public static final String DEFAULT_STORAGE_FILE = "messages.data";
	
}