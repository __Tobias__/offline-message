package org.tpc.minecraft.offlinemessage;

import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.regex.Pattern;

import javax.persistence.PersistenceException;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.tpc.minecraft.offlinemessage.data.Message;

import static org.tpc.minecraft.offlinemessage.Constants.*;

public class OfflineMessage extends JavaPlugin implements Listener {
	
	private static final Pattern MESSAGE_RETRIEVE_PATTERN = Pattern.compile("^\\b(.+)\\b\\?$");
	
	private MessageStore messageStore;
	
	//#ifndef mvn.project.property.offlinemessage.database
	private Path dataFile;
	//#endif
	
	private boolean removeMessageOnJoin;
	private boolean showIfOnline;
	private long messageStorageTime;
	private long messageCleanupTime;
	private Map<String, String> messages;
	
	public OfflineMessage() {
		messageStore = new MessageStore(this);
		messages = new HashMap<>();
	}
	
	@Override
	public void onLoad() {
		saveDefaultConfig();
		
		FileConfiguration config = getConfig();
		messages.put(CONFIG_MESSAGE_RETRIEVE, config.getString(CONFIG_MESSAGE_RETRIEVE, DEFAULT_MESSAGE_RETRIEVE));
		messages.put(CONFIG_MESSAGE_SET, config.getString(CONFIG_MESSAGE_SET, DEFAULT_MESSAGE_SET));
		messages.put(CONFIG_MESSAGE_REMOVE, config.getString(CONFIG_MESSAGE_REMOVE, DEFAULT_MESSAGE_REMOVE));
		messages.put(CONFIG_MESSAGE_REMOVE_ERROR, config.getString(CONFIG_MESSAGE_REMOVE_ERROR, DEFAULT_MESSAGE_REMOVE_ERROR));
		messages.put(CONFIG_MESSAGE_SHOW, config.getString(CONFIG_MESSAGE_SHOW, DEFAULT_MESSAGE_SHOW));
		messages.put(CONFIG_MESSAGE_SHOW_ERROR, config.getString(CONFIG_MESSAGE_SHOW_ERROR, DEFAULT_MESSAGE_SHOW_ERROR));
		
		removeMessageOnJoin = config.getBoolean(CONFIG_MESSAGE_REMOVE_ON_JOIN, DEFAULT_MESSAGE_REMOVE_ON_JOIN);
		messageStorageTime = config.getLong(CONFIG_MESSAGE_STORAGE_TIME, DEFAULT_MESSAGE_STORAGE_TIME);
		messageCleanupTime = config.getLong(CONFIG_MESSAGE_CLEANUP_TIME, DEFAULT_MESSAGE_CLEANUP_TIME);
		showIfOnline = config.getBoolean(CONFIG_MESSAGE_SHOW_IF_ONLINE, DEFAULT_MESSAGE_SHOW_IF_ONLINE);
		
		//#ifndef mvn.project.property.offlinemessage.database
		try {
			dataFile = getDataFolder().toPath().resolve(config.getString(CONFIG_STORAGE_FILE, DEFAULT_STORAGE_FILE));
		} catch (InvalidPathException pathException) {
			dataFile = getDataFolder().toPath().resolve(DEFAULT_STORAGE_FILE);
		}
		//#endif
		
		messageStore.setStoreTime(messageStorageTime);
		messageStore.setClearTime(messageCleanupTime);
	}
	
	@Override
	public void onEnable() {
		//#ifdef mvn.project.property.offlinemessage.database
		try {
			getDatabase().find(Message.class).findRowCount();
		} catch (PersistenceException persistenceException) {
			installDDL();
			getLogger().log(Level.INFO, "Initializing database for first use!");
		}
		//#endif
		
		messageStore.activate();
		
		getServer().getPluginManager().registerEvents(this, this);
	}
	
	@Override
	public void onDisable() {
		messageStore.deactivate();
	}
	
	//#ifdef mvn.project.property.offlinemessage.command
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(!isEnabled()) {
			return false;
		}
		
		if(!(sender instanceof Player)) {
			sender.sendMessage("Can't use this command from console!");
			return false;
		}
		
		if(args.length == 0) {
			processCommandSet((Player) sender, null);
			return true;
		}
		
		if(args.length == 1 && "show".equalsIgnoreCase(args[0])) {
			processCommandShow((Player) sender);
			return true;
		}
		
		StringBuilder sb = new StringBuilder();
		for(int index = 0; index < args.length; index++) {
			if(index != 0) {
				sb.append(' ');
			}
			
			sb.append(args[index]);
		}
		
		processCommandSet((Player) sender, sb.toString());
		return true;
	}
	//#endif
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent chatEvent) {
		if(!isEnabled()) {
			return;
		}
		
		String message = chatEvent.getMessage();
		Player player = chatEvent.getPlayer();
		
		//#ifdef mvn.project.property.offlinemessage.command
		processCommandRetrieve(player, message);
		//#else
		String lower = message.toLowerCase();
		if(lower.equalsIgnoreCase("offmsg")) {
			processCommandSet(player, null);
			chatEvent.setCancelled(true);
		} else if(lower.startsWith("offmsg ")) {
			if((message = message.substring(6).trim()).equalsIgnoreCase("show")) {
				processCommandShow(player);
			} else {
				processCommandSet(player, message);
			}
			
			chatEvent.setCancelled(true);
		} else {
			processCommandRetrieve(player, message);
		}
		//#endif
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		if(!removeMessageOnJoin || !isEnabled()) {
			return;
		}
		
		// Not using java 8 Optional yet (backwards compatibility)
		Player player = event.getPlayer();
		if(player == null) {
			return;
		} 
		
		UUID uuid = player.getUniqueId();
		if(uuid == null) {
			return;
		}
		
		messageStore.removeMessage(uuid);
	}
	
	private boolean processCommandRetrieve(Player sender, String text) {
		if(!MESSAGE_RETRIEVE_PATTERN.matcher(text).matches()) {
			return false;
		}
		
		String playerName = text.substring(0, text.length() - 1);
		if(!showIfOnline && (getServer().getPlayer(playerName) != null)) {
			return false; // Player is online
		}
		
		@SuppressWarnings("deprecation")
		OfflinePlayer player = getServer().getOfflinePlayer(playerName);
		if(player == null) {
			return false; // No player found
		}
		
		UUID uuid = player.getUniqueId();
		if(uuid == null) {
			return false;
		}
		
		Message message = messageStore.getMessage(uuid);
		if(message == null) {
			return false;
		}
		
		sender.sendMessage(String.format(messages.get(CONFIG_MESSAGE_RETRIEVE), player.getName(), message.getMessage()));
		return true;
	}
	
	private void processCommandSet(Player sender, String message) {
		UUID uuid = sender.getUniqueId();
		if(uuid == null) {
			sender.sendMessage(messages.get(CONFIG_MESSAGE_REMOVE_ERROR));
			return;
		}
		
		if(message == null) {
			Message messageObj = messageStore.getMessage(uuid);
			if(messageObj == null) {
				sender.sendMessage(messages.get(CONFIG_MESSAGE_REMOVE_ERROR));
				return;
			}
			
			messageStore.removeMessage(uuid);
			sender.sendMessage(messages.get((messageObj.getCreationTime() < (System.currentTimeMillis() - messageStorageTime))
					? CONFIG_MESSAGE_REMOVE_ERROR : CONFIG_MESSAGE_REMOVE));
		} else {
			messageStore.storeMessage(new Message(uuid, message));
			sender.sendMessage(String.format(messages.get(CONFIG_MESSAGE_SET), message));
		}
	}
	
	private void processCommandShow(Player sender) {
		UUID uuid = sender.getUniqueId();
		if(uuid == null) {
			sender.sendMessage(messages.get(CONFIG_MESSAGE_SHOW_ERROR));
			return;
		}
		
		Message message = messageStore.getMessage(uuid);
		if(message == null) {
			sender.sendMessage(messages.get(CONFIG_MESSAGE_SHOW_ERROR));
			return;
		}
		
		sender.sendMessage(String.format(messages.get(CONFIG_MESSAGE_SHOW), message.getMessage(), sender.getName()));
	}
	
	public long getMessageStorageTime() {
		return messageStorageTime;
	}
	
	//#ifdef mvn.project.property.offlinemessage.database
	@Override
	public List<Class<?>> getDatabaseClasses() {
		return Arrays.asList(Message.class);
	}
	
	//#else
	public Path getDataFile() {
		return dataFile;
	}
	//#endif
}