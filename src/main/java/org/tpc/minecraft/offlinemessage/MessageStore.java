package org.tpc.minecraft.offlinemessage;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.PersistenceException;

import org.apache.commons.io.IOUtils;
import org.tpc.minecraft.offlinemessage.data.Message;

import com.avaje.ebean.EbeanServer;
import com.avaje.ebean.QueryIterator;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

public class MessageStore {
	
	private OfflineMessage plugin;
	private Logger logger;
	
	private ScheduledExecutorService cleanupExecutor;
	private long storeTime;
	private long clearTime;
	
	//#ifdef mvn.project.property.offlinemessage.database
	private EbeanServer database;
	//#else
	private Map<UUID, Message> messages;
	//#endif
	
	public MessageStore(OfflineMessage plugin) {
		this.plugin = plugin;
		logger = plugin.getLogger();
		
		//#ifdef mvn.project.property.offlinemessage.database
		database = plugin.getDatabase();
		//#else
		messages = new ConcurrentHashMap<>();
		//#endif
	}
	
	public void activate() {
		cleanupExecutor = Executors.newSingleThreadScheduledExecutor(new ThreadFactoryBuilder().setDaemon(true).build());
		cleanupExecutor.scheduleAtFixedRate(this::clean, clearTime, clearTime, TimeUnit.MILLISECONDS);
		
		//#ifndef mvn.project.property.offlinemessage.database
		Path dataPath = plugin.getDataFile();
		if(Files.isRegularFile(dataPath)) {
			readDataFromFile(dataPath);
		}
		//#endif
	}
	
	public void deactivate() {
		ExecutorService executorService = cleanupExecutor;
		if((executorService != null) && (!executorService.isShutdown())) {
			executorService.shutdownNow();
		}
		
		//#ifndef mvn.project.property.offlinemessage.database
		writeDataToFile(plugin.getDataFile());
		//#endif
	}
	
	public Message getMessage(UUID id) {
		Message message;
		//#ifdef mvn.project.property.offlinemessage.database
		try {
			message = database.find(Message.class, id);
		} catch(PersistenceException e) {
			logger.log(Level.WARNING, String.format("Could not retrieve message of %s from database!", id));
			return null;
		}
		//#else
		message = messages.get(id);
		//#endif
		
		return message;
	}
	
	public void storeMessage(Message message) {
		//#ifdef mvn.project.property.offlinemessage.database
		try {
			Message messageInDB = database.find(Message.class, message.getId());
			if(messageInDB != null) {
				messageInDB.setCreationTime(message.getCreationTime());
				messageInDB.setMessage(message.getMessage());
				database.save(messageInDB);
			} else {
				database.insert(message);
			}
		} catch(PersistenceException e) {
			logger.log(Level.WARNING, String.format("Could not set message of %s in database!", message.getId()));
		}
		//#else
		messages.put(message.getId(), message);
		//#endif
	}
	
	public void removeMessage(UUID id) {
		//#ifdef mvn.project.property.offlinemessage.database
		try {
			database.delete(Message.class, id);
		} catch(PersistenceException e) {
			logger.log(Level.WARNING, String.format("Could not delete message of %s from database!", id));
		}
		//#else
		messages.remove(id);
		//#endif
	}
	
	protected void clean() {
		long startTime = System.currentTimeMillis();
		int deleted = 0;
		
		logger.log(Level.FINE, "Clearing out old messages ...");
		
		//#ifdef mvn.project.property.offlinemessage.database
		QueryIterator<Message> iterator = null;
		try {
			iterator = database.createQuery(Message.class).select("id,creationTime").where(
					database.getExpressionFactory().le("creationTime", System.currentTimeMillis() - storeTime)).findIterate();
			
			deleted = database.delete(iterator);
		} catch(PersistenceException e) {
			logger.log(Level.WARNING, "Could not clean up messages in database!", e);
		} finally {
			IOUtils.closeQuietly(iterator);
		}
		//#else
		for(Message message : messages.values()) {
			if(message.getCreationTime() < System.currentTimeMillis() - storeTime) {
				messages.remove(message.getId());
				deleted++;
			}
		}
		//#endif
		
		logger.log(Level.FINE, String.format("Cleared %d messages in %d ms!", deleted, (System.currentTimeMillis() - startTime)));
	}
	
	//#ifndef mvn.project.property.offlinemessage.database
	private void readDataFromFile(Path path) {
		try(InputStream inStream = Files.newInputStream(path);
				ObjectInputStream in = new ObjectInputStream(inStream)) {
			
			for(;;) {
				UUID uuid = (UUID) in.readObject();
				Message message = (Message) in.readObject();
				if(message.getCreationTime() < System.currentTimeMillis() - storeTime) {
					continue;
				}
				
				messages.put(uuid, message);
			}
		} catch (EOFException eofException) {
			; // Ignore
		} catch (IOException | ClassNotFoundException ioException) {
			plugin.getLogger().log(Level.WARNING, "Could not load saved messages!", ioException);
		}
	}
	
	private void writeDataToFile(Path path) {
		try(OutputStream outStream = Files.newOutputStream(path, StandardOpenOption.CREATE);
				ObjectOutputStream out = new ObjectOutputStream(outStream)) {
			
			for(Entry<UUID, Message> messageEntry : messages.entrySet()) {
				Message message = messageEntry.getValue();
				if(message.getCreationTime() < System.currentTimeMillis() - storeTime) {
					continue;
				}
				
				out.writeObject(messageEntry.getKey());
				out.writeObject(message);
			}
		} catch (IOException ioException) {
			plugin.getLogger().log(Level.WARNING, "Could not save messages!", ioException);
		}
	}
	//#endif
	
	public void setStoreTime(long storeTime) {
		this.storeTime = storeTime;
	}
	
	public long getStoreTime() {
		return storeTime;
	}
	
	public void setClearTime(long clearTime) {
		this.clearTime = clearTime;
	}
	
	public long getClearTime() {
		return clearTime;
	}
}